## v2.0.0

change arduino-builder to arduino-cli.

**Tool list**

- Arduino

    Support all basic avr arduino board.

- Python

    With uflash and microfs inside, for microbit.

**Support system**

- Windows x86/x64
- macOS x64

## v1.2.0

Add arch parameter.

- **Tool list**

    - Arduino

        Support all basic avr arduino board.

    - Python

        With uflash and microfs inside, for microbit.
    
- **Support system**

    - Windows x86/x64
    - macOS x64

## v1.1.0

**Tool list**

- Arduino

	Support all basic avr arduino board.

- Python3.9.2 (32-bit)

	With uflash and microfs inside, for microbit.

**Support system**

- Windows x86/x64

## v1.0.0

**Tool list**

- Arduino

	Support all basic avr arduino board.

- python-uflash

	For microbit.

**Support system**

- windows
